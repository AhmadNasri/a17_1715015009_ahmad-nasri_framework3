@extends('templates.home')
@section('title')
	Show Order
@endsection
@section('content')

	<h1>Detail Order</h1>
	<hr>
	<br>

	<div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">

		<div class="row">
			<div class="col-md-12 text-center">
				<h3>Invoice : {{ $orders['invoice_number'] }} </h3>
			</div>
		</div>
		<hr>

		<br>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Nama
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $orders->users->username }}
			</div>
			<br>
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Total Harga
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $orders->total_price }}
			</div>		
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Status
			</div>
			<div class="col-md-4 col-sm-4">
				@if ( $orders['status'] == 'PROCESS')
				<div class="p-1 mb-2 bg-warning text-dark">{{ $orders['status'] }}</div>
				@elseif ( $orders['status'] == "CANCEL")
				<div class="p-1 mb-2 bg-danger text-white">{{ $orders['status'] }}</div>
				@elseif ( $orders['status'] == "SUBMIT")
					<div class="p-1 mb-2 bg-primary text-white">{{ $orders['status'] }}</div>
				@elseif ( $orders['status']  == "FINISH")
					<div class="p-1 mb-2 bg-success text-white">{{ $orders['status'] }}</div>
				@endif
			</div>		
		</div>

		

	</div>

@endsection