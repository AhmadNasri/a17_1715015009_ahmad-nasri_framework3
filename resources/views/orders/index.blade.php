@extends('templates.home')
@section('title')
	Daftar Order
@endsection
@section('css')
	<style>
		body{
			padding-top: 30px;
		}
		th, td {
			padding: 10px;
			text-align: center;
		}
		td a{
			margin: 3px;
			align-content: center;
			color: white;
		}
		td a:hover{
			text-decoration: none;
		}
		td button{
			margin-top: 5px;
			cursor: pointer;
		}
	</style>
@endsection
@section('content')
	<div class="container">
		<h1>Orders List</h1>
		<div class="row">
			<div class="col-md-2">
				<a class="btn btn-outline-primary" href="{{ route('orders.create') }}"><span data-feather="plus-circle"></span> Create New Order <span class="sr-only">(current)</span></a>
			</div>
			<div class="col-md-8">
			<form action="{{ route('orders.search') }}" class="form-inline" method="GET">
				<div class="form-group mx-sm-3 mb-2">
				<input class="form-control" name="cari" placeholder="Cari Pesanan...." value="{{ old('cari') }}">
				</div>
				<button class="btn btn-primary mb-2" type="submit">Cari</button>
			</form>
			</div>
		</div>
		<br>
		<div class="table-responsive">
		<table class="table table-striped">
		<thead>
			<tr class="table-primary">
				<th scope="col">Id</th>
				<th scope="col">Nama</th>
				<th scope="col">Total Harga</th>
				<th scope="col">Invoice</th>
				<th scope="col">Status</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($orders as $order)
		<tr>
			<td>{{ $order->id }}</td>
			<td>{{ $order->users->username }}</td>
			<td>{{ $order->total_price }}</td>
			<td>{{ $order->invoice_number }}</td>
			<td>
			@if ( $order['status'] == 'PROCESS')
			<div class="p-1 mb-2 bg-warning text-dark">{{ $order['status'] }}</div>
			@elseif ( $order['status'] == "CANCEL")
			<div class="p-1 mb-2 bg-danger text-white">{{ $order['status'] }}</div>
			@elseif ( $order['status'] == "SUBMIT")
				<div class="p-1 mb-2 bg-primary text-white">{{ $order['status'] }}</div>
			@elseif ( $order['status']  == "FINISH")
				<div class="p-1 mb-2 bg-success text-white">{{ $order['status'] }}</div>
			@endif
			</td>
			<td>
				<a class="btn-sm btn-primary" href="{{ route('orders.show', ['id'=>$order['id']]) }}"><span data-feather="eye"></span>Detail<span class="sr-only">(current)</span></a>
				<a class="btn-sm btn-success d-inline" href="{{ route('orders.edit', ['id'=>$order['id']]) }}"><span data-feather="edit-2"></span>Edit<span class="sr-only">(current)</span></a>

				<form class="d-inline" onsubmit="return confirm('DELETE this order permanetly?')" action="{{ route('orders.destroy', ['id'=>$order['id']]) }}" method="POST">
					@csrf
					@method('DELETE')
					<button type="submit" class="btn-sm btn-danger" value="Delete" name="submit"><span data-feather="trash"></span>Delete<span class="sr-only">(current)</span></button>
				</form>

			</td>
		</tr>
		@endforeach
		</tbody>
	</table>

	{{ $orders->links() }}
</div>
</div>
@endsection