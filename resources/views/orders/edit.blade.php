@extends('templates.home')
@section('title')
	Edit Order
@endsection
@section('content')

	<div class="container">
		<h1>Edit Order</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">

		<div class="card-header bg-primary text-white">
			<h5> Edit Order Detail </h5>
		</div>

		<div class="card-body">

		<div class="container text-primary">

			<form action="{{ route('orders.update', $orders['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">
				@csrf
				@method('PUT')

				    <div class="row">
						<div class="col-md-3">
							<label class="text-primary" for="invoice">Invoice</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="text" name="invoice_number" id="invoice" value="{{ $orders['invoice_number'] }}">
							{{ ($errors->has('invoice_number')) ? $errors->first('invoice_number') : "" }}
						</div>
					</div>

					<br>

					<div class="row">
						<div class="col-md-3">
							<label  for="nama">Nama</label>
						</div>
						<div class="col-md-8">
							<select class="form-control" name="user_id" id="status">
								@foreach ($users as $user)
									<option value="{{ $user->id }}">{{ $user->username }}</option>
								@endforeach
							</select>
							{{ ($errors->has('user_id')) ? $errors->first('user_id') : "" }}
						</div>
					</div>

					<br>

					<div class="row">
						<div class="col-md-3">
							<label for="total" class="text-primary">Total Harga	</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="text" name="total_price" id="total" value="{{ $orders['total_price'] }}">
							{{ ($errors->has('total_price')) ? $errors->first('total_price') : "" }}
						</div>
					</div>

					<br>

					<div class="row">
						<div class="col-md-3">
							<label for="status">Status</label>
						</div>
						<div class="col-md-8">
							<select class="form-control" name="status" id="status">
								<option value="SUBMIT">SUBMIT</option>
								<option value="PROCESS">PROCESS</option>
								<option value="FINISH">FINISH</option>
								<option value="CANCEL">CANCEL</option>
							</select>
							{{ ($errors->has('status')) ? $errors->first('status') : "" }}
						</div>
					</div>

					<br>

					
				<br>
					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Simpan</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection