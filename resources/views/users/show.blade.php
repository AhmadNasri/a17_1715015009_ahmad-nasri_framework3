@extends('templates.home')
@section('title')
	Show User
@endsection
@section('content')

	<h1>User Detail</h1>
	<hr>
	<br>

	<div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">

		<div class="row" style="padding: 25px;">
			<div class="col-md-2 offset-md-4">
				<img src=" {{ asset($users->avatar) }} " alt="gambar" style="width:300px; height: 260px;" class="rounded">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 text-center">
				<h3>{{ $users->username }} </h3>
			</div>
		</div>
		<hr>

		<br>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Alamat
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $users->address }}
			</div>
			<br>
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Email
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $users->email }}
			</div>
			<br>
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				No. HP
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $users->phone }}
			</div>		
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Status
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $users->status }}
			</div>		
		</div>

	</div>

@endsection