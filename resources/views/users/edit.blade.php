@extends('templates.home')
@section('title')
	Edit User
@endsection
@section('content')

	<div class="container">
		<h1>Edit User</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">

		<div class="card-header bg-primary text-white">
			<h5> Edit User Detail </h5>
		</div>

		<div class="card-body">

		<div class="container text-primary">
			<form action="{{ route('users.update', ['id'=>$users->id]) }}" method="POST" class="form-group" enctype="multipart/form-data">
				@csrf
				@method('PUT')

				    <div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="nama">Nama</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama" id="nama" value="{{ $users->username }}">
						{{ ($errors->has('nama')) ? $errors->first('nama') : "" }}
					</div>
					</div>

					<br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="alamat">Alamat</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="alamat" id="alamat" value="{{ $users->address }}">
						{{ ($errors->has('alamat')) ? $errors->first('alamat') : "" }}
					</div>
					</div>					

					<br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="email">Email</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="email" name="email" id="email" value="{{ $users->email }}">
						{{ ($errors->has('email')) ? $errors->first('email') : "" }}
					</div>
					</div>
					
					<br>

					<div class="row">
						<div class="col-md-3">
							<label for="telepon" class="text-primary">No. HP</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="number" name="telepon" id="telepon" value="{{ $users->phone }}">
							{{ ($errors->has('telepon')) ? $errors->first('telepon') : "" }}
						</div>
					</div>

					<br>

					<div class="row">
						<div class="input-group mb-3">
							<div class="col-md-3 text-primary">
								Avatar
							</div>

							<div class="col-md-8">
								<img src=" {{asset($users->avatar)}} " alt="gambar" class="img-thumbnail" height="150px" width="150px">
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="avatar" id="customFile">
									  <label class="custom-file-label" for="customFile">Upload Gambar</label>
									</div>
									{{ ($errors->has('avatar')) ? $errors->first('avatar') : "" }}
							</div>
						</div>
					</div>

					<br>

					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Simpan</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection