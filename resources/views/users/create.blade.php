@extends('templates.home')
@section('title')
	Create User
@endsection

@section('content')
	
	<div class="container">
		<h1>Create User</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">
			<div class="card-header bg-primary text-white">
				<h5>Create a New User</h5>
			</div>
			<div class="card-body">
				<div class="container text-primary">

				<form class="form-group" action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-md-3">
						<label  for="nama">Nama</label>
					</div>
					<div class="col-md-8">
					<input class="form-control" type="text" name="nama" id="nama" value="{{ old('nama') }}">
					{{ ($errors->has('nama')) ? $errors->first('nama') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label for="email">Email</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="email" name="email" id="email"  
						value="{{ old('email') }}" cols="30" rows="10">
					{{ ($errors->has('email')) ? $errors->first('email') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label for="password">Password</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="password" name="password" id="password"  
						value="{{ old('password') }}" cols="30" rows="10">
					{{ ($errors->has('password')) ? $errors->first('password') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label for="alamat">Alamat</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="alamat" id="alamat" 
						value="{{ old('alamat') }}">
					{{ ($errors->has('alamat')) ? $errors->first('alamat') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label for="telepon">No. HP</label>
					</div>
					<div class="col-md-8">
						<input type="number" name="telepon" id="telepon" value="{{ old('telepon') }}">
					{{ ($errors->has('telepon')) ? $errors->first('telepon') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="input-group mb-3">
						<div class="col-md-3 text-primary">
							Gambar
						</div>
					<div class="col-md-8">
						<div class="custom-file">
							<label for="avatar" class="custom-file-label">Upload Gambar</label>
							<input class="custom-file-input" type="file" name="avatar" id="avatar" value="{{ old('avatar') }}">
						{{ ($errors->has('avatar')) ? $errors->first('avatar') : "" }}
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3 offset-md-5 offset-md-4">
					<button class="btn btn-outline-primary" type="submit">Simpan</button>
				</div>
			</div>
		</form>
	</div>
	</div>
</div>
</div>
@endsection