@extends('templates.home')
@section('title')
	Daftar User
@endsection
@section('css')
	<style>
		body{
			padding-top: 30px;
		}
		th, td {
			padding: 10px;
			text-align: center;
		}
		td a{
			margin: 3px;
			align-content: center;
			color: white;
		}
		td a:hover{
			text-decoration: none;
		}
		td button{
			margin-top: 5px;
			cursor: pointer;
		}
	</style>
@endsection
@section('content')

	<div class="container">
		<h1>Users List</h1>
		<div class="row">
			<div class="col-md-2">
				<a class="btn btn-outline-primary" href="{{ route('users.create') }}"><span data-feather="plus-circle"></span> Create User <span class="sr-only">(current)</span></a>
			</div>
			<div class="col-md-8">
			<form action="{{ route('users.search') }}" class="form-inline" method="GET">
				<div class="form-group mx-sm-3 mb-2">
				<input class="form-control" name="cari" placeholder="Cari e-mail...." value="{{ old('cari') }}">
				</div>
				<button class="btn btn-primary mb-2" type="submit">Cari</button>
			</form>
			</div>
		</div>
		<br>
		<div class="table-responsive">
		<table class="table table-striped">
		<thead>
			<tr class="table-primary">
				<th scope="col">ID</th>
				<th scope="col">Nama</th>
				<th scope="col">Email</th>
				<th scope="col">No. HP</th>
				<th scope="col">Gambar</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($users as $user)
		<tr>
			<td>{{ $user->id }}</td>
			<td>{{ $user->username }}</td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->phone }}</td>
			<td><img src="{{ asset($user->avatar) }}" alt="avatar" width="100px" height="80px"></td>

			<td>
				<a class="btn-sm btn-primary" href="{{ route('users.show', ['id'=>$user->id]) }}"><span data-feather="eye"></span>Detail<span class="sr-only">(current)</span></a>
				<a class="btn-sm btn-success d-inline" href="{{ route('users.edit', ['id'=>$user->id]) }}"><span data-feather="edit-2"></span>Edit<span class="sr-only">(current)</span></a>

				<form class="d-inline" onsubmit="return confirm('DELETE this user permanetly?')" action="{{ route('users.destroy', ['id'=>$user->id]) }}" method="POST">
					@csrf
					@method('DELETE')
					<button type="submit" class="btn-sm btn-danger" value="Delete" name="submit"><span data-feather="trash"></span>Delete<span class="sr-only">(current)</span></button>
				</form>

			</td>
		</tr>
		@endforeach
		</tbody>
	</table>

	{{ $users->links() }}
</div>
</div>
@endsection