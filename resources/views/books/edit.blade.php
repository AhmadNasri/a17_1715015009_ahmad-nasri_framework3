@extends('templates.home')
@section('title')
	Edit Buku
@endsection
@section('content')

	<div class="container">
		<h1>Edit Book</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">

		<div class="card-header bg-primary text-white">
			<h5>Edit Book Detail </h5>
		</div>

		<div class="card-body">

		<div class="container text-primary">

			<form action="{{ route('books.update', $books['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">
				@csrf
				@method('PUT')

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="title">Judul</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="title" id="title" value="{{ $books['title'] }}">
						{{ ($errors->has('title')) ? $errors->first('title') : "" }}
					</div>
					</div>
					<br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="author">Pengarang</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="author" id="author" value="{{ $books['author'] }}">
						{{ ($errors->has('author')) ? $errors->first('author') : "" }}
					</div>
					</div>
					<br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="publisher">Penerbit</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="publisher" id="publisher" value="{{ $books['publisher'] }}">
						{{ ($errors->has('publisher')) ? $errors->first('publisher') : "" }}
					</div>
					</div>
					<br>

					<div class="row">
						<div class="col-md-3">
							<label for="price" class="text-primary">Harga</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="text" name="price" id="price" value="{{ $books['price'] }}">
							{{ ($errors->has('price')) ? $errors->first('price') : "" }}
						</div>
					</div>
					<br>

					<div class="row">
						<div class="col-md-3">
							<label for="stock" class="text-primary">Stock</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="text" name="stock" id="stock" value="{{ $books['stock'] }}">
							{{ ($errors->has('stock')) ? $errors->first('stock') : "" }}
						</div>
					</div>
					<br>

					<div class="row">
						<div class="col-md-3">
							<label for="description" class="text-primary">Deskripsi</label>
						</div>
						<div class="col-md-8">
							<input class="form-control" type="textarea" name="description" id="description" 
							value="{{ $books['description'] }}">
							{{ ($errors->has('description')) ? $errors->first('description') : "" }}
						</div>
					</div>
					<br>

					<div class="row">
						<div class="col-md-3">
							<label for="category_name" class="text-primary">Genre</label>
						</div>
						<div class="col-md-8">
							<select multiple class="form-control" name="category_name[]" id="category_name">
								@foreach ($categories as $category)
									<option value="{{ $category->id }}">{{ $category->category_name }}</option>
								@endforeach
							</select>
							{{ ($errors->has('category_name')) ? $errors->first('category_name') : "" }}
						</div>
					</div>
					<br>

					<div class="row">
						<div class="input-group mb-3">
							<div class="col-md-3 text-primary">
								Cover
							</div>

							<div class="col-md-8">
								<img src=" {{asset($books['cover'])}} " alt="image" class="img-thumbnail" height="150px" width="150px">
								<div class="custom-file">
								<input type="file" class="custom-file-input" name="cover" id="customFile">
  									<label class="custom-file-label" for="customFile">Upload Gambar</label>
								</div>
								{{ ($errors->has('cover')) ? $errors->first('cover') : "" }}
							</div>
						</div>
					</div>

					<br>

					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Update</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection