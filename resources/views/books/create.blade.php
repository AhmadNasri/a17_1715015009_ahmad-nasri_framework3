@extends('templates.home')
@section('title')
	Create Book
@endsection

@section('content')
	
	<div class="container">
		<h1>Create Book</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">
			<div class="card-header bg-primary text-white">
				<h5>Create a New Book</h5>
			</div>
			<div class="card-body">
				<div class="container text-primary">

				<form class="form-group" action="{{ route('books.store') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-md-3">
						<label for="title">Judul</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="title" id="title">
						{{ ($errors->has('title')) ? $errors->first('title') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label  for="author">Pengarang</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="author" id="author">
						{{ ($errors->has('author')) ? $errors->first('author') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label  for="publisher">Penerbit</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="publisher" id="publisher">
						{{ ($errors->has('publisher')) ? $errors->first('publisher') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label  for="price">Harga</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="price" id="price">
						{{ ($errors->has('price')) ? $errors->first('price') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label  for="stock">Stock</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="stock" id="stock">
						{{ ($errors->has('stock')) ? $errors->first('stock') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label  for="description">Description</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="textarea" name="description" id="description">
						{{ ($errors->has('description')) ? $errors->first('description') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label  for="category_name">Genre</label>
					</div>
					<div class="col-md-8">
						<select multiple class="form-control" name="category_name[]" id="category_name">
							@foreach ($categories as $category)
								<option value="{{ $category->id }}">{{ $category->category_name }}</option>
							@endforeach
						</select>
						{{ ($errors->has('category_name')) ? $errors->first('category_name') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="input-group mb-3">
						<div class="col-md-3 text-primary">
							Gambar
						</div>
					<div class="col-md-8">
						<div class="custom-file">
							<label for="gambar" class="custom-file-label">Gambar</label>
							<input class="custom-file-input" type="file" name="cover" id="gambar">
						</div>
						{{ ($errors->has('cover')) ? $errors->first('cover') : "" }}
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3 offset-md-5 offset-md-4">
					<button class="btn btn-outline-primary" type="submit">Create</button>
				</div>
			</div>
		</form>
	</div>
	</div>
</div>
</div>
@endsection