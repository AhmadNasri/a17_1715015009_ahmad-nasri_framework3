@extends('templates.home')
@section('title')
	Daftar Buku
@endsection
@section('css')
	<style>
		body{
			padding-top: 30px;
		}
		th, td {
			padding: 10px;
			text-align: center;
		}
		td a{
			margin: 3px;
			align-content: center;
			color: white;
		}
		td a:hover{
			text-decoration: none;
		}
		td button{
			margin-top: 5px;
			cursor: pointer;
		}
	</style>
@endsection
@section('content')
	
	<div class="container">
		<h1>List Book </h1>
		<div class="row">
			<div class="col-md-2">
				<a class="btn btn-outline-primary" href="{{ route('books.create') }}"><span data-feather="plus-circle"></span>Create New Book<span class="sr-only">(current)</span></a>
			</div>
			<div class="col-md-8">
			<form action="{{ route('books.search') }}" class="form-inline" method="GET">
				<div class="form-group mx-sm-3 mb-2">
				<input class="form-control" name="cari" placeholder="Cari Buku...." value="{{ old('cari') }}">
				</div>
				<button class="btn btn-primary mb-2" type="submit">Cari</button>
			</form>
			</div>
		</div>
		<br>
		<div class="table-responsive">
		<table class="table table-striped">
		<thead>
			<tr class="table-primary">
				<th scope="col">ID</th>
				<th scope="col">Judul</th>
				<th scope="col">Pengarang</th>
				<th scope="col">Harga</th>
				<th scope="col">Cover</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($books as $book)
		<tr>
			<td>{{ $book['id'] }}</td>
			<td>{{ $book['title'] }}</td>
			<td>{{ $book['author'] }}</td>
			<td>{{ $book['price'] }}</td>
			<td><img src="{{ asset($book['cover']) }}" alt="image" width="100px" height="80px"></td>
			<td>
				<a class="btn-sm btn-primary" href="{{ route('books.show', ['id'=>$book['id']]) }}"><span data-feather="eye"></span>Detail<span class="sr-only">(current)</span></a>
				<a class="btn-sm btn-success d-inline" href="{{ route('books.edit', ['id'=>$book['id']]) }}"><span data-feather="edit-2"></span>Edit<span class="sr-only">(current)</span></a>

				<form class="d-inline" onsubmit="return confirm('DELETE this user permanetly?')" action="{{ route('books.destroy', ['id'=>$book['id']]) }}" method="POST">
					@csrf
					@method('DELETE')
					<button type="submit" class="btn-sm btn-danger" value="Delete" name="submit"><span data-feather="trash"></span>Delete<span class="sr-only">(current)</span></button>
				</form>
			</td>
		</tr>
		@endforeach
		</tbody>
	</table>

	{{ $books->links() }}
</div>
</div>
@endsection

