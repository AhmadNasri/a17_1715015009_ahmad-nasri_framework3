@extends('templates.home')
@section('title')
	Show Buku
@endsection
@section('content')

	<h1>Book Detail</h1>
	<hr>
	<br>

	<div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">

		<div class="row" style="padding: 25px;"">
			<div class="col-md-2 offset-md-4">
				<img src=" {{ asset($books['cover']) }} " alt="gambar" style="width:300px; height: 260px;" class="rounded">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 text-center">
				<h3> {{ $books['title'] }} </h3>
			</div>
		</div>
		<hr>

		<br>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Pengarang
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $books['author'] }}
			</div>
			<br>
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Penerbit
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $books['publisher'] }}
			</div>		
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Deskripsi
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $books['description'] }}
			</div>		
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Genre
			</div>
			<div class="col-md-4 col-sm-4">
				<ul>
				@foreach ($books->category as $category)
					<li>{{ $category->category_name }}</li>
				@endforeach
				</ul>
			</div>		
		</div>

	</div>

