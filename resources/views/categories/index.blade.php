@extends('templates.home')
@section('title')
	Daftar Kategori
@endsection
@section('css')
	<style>
		body{
			padding-top: 30px;
		}
		th, td {
			padding: 10px;
			text-align: center;
		}
		td a{
			margin: 3px;
			align-content: center;
			color: white;
		}
		td a:hover{
			text-decoration: none;
		}
		td button{
			margin-top: 5px;
			cursor: pointer;
		}
	</style>
@endsection
@section('content')
	
	<div class="container">
		<h1>Categories List</h1>
		<div class="row">
			<div class="col-md-2">
				<a class="btn btn-outline-primary" href="{{ route('categories.create') }}"><span data-feather="plus-circle"></span>Create New Category<span class="sr-only">(current)</span></a>
			</div>
			<div class="col-md-8">
			<form action="{{ route('categories.search') }}" class="form-inline" method="GET">
				<div class="form-group mx-sm-3 mb-2">
				<input class="form-control" name="cari" placeholder="Cari genre...." value="{{ old('cari') }}">
				</div>
				<button class="btn btn-primary mb-2" type="submit">Cari</button>
			</form>
			</div>
		</div>
		<br>
		<div class="table-responsive">
		<table class="table table-striped">
		<thead>
			<tr class="table-primary">
				<th scope="col">ID</th>
				<th scope="col">Genre</th>
				<th scope="col">Deskripsi</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($categories as $kategori)
		<tr>
			<td>{{ $kategori['id'] }}</td>
			<td>{{ $kategori['category_name'] }}</td>
			<td>{{ $kategori['description'] }}</td>
			<td>
				<a class="btn-sm btn-primary" href="{{ route('categories.show', ['id'=>$kategori['id']]) }}"><span data-feather="eye"></span>Detail<span class="sr-only">(current)</span></a>
				<a class="btn-sm btn-success d-inline" href="{{ route('categories.edit', ['id'=>$kategori['id']]) }}"><span data-feather="edit-2"></span>Edit<span class="sr-only">(current)</span></a>

				<form class="d-inline" onsubmit="return confirm('DELETE this user permanetly?')" action="{{ route('categories.destroy', ['id'=>$kategori['id']]) }}" method="POST">
					@csrf
					@method('DELETE')
					<button type="submit" class="btn-sm btn-danger" value="Delete" name="submit"><span data-feather="trash"></span>Delete<span class="sr-only">(current)</span></button>
				</form>

			</td>
		</tr>
		@endforeach
		</tbody>
	</table>

	{{ $categories->links() }}
</div>
</div>
@endsection