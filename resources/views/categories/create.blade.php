@extends('templates.home')
@section('title')
	Create Kategori
@endsection

@section('content')
	
	<div class="container">
		<h1>Create Kategori</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">
			<div class="card-header bg-primary text-white">
				<h5>Create a New Kategori</h5>
			</div>
			<div class="card-body">
				<div class="container text-primary">

				<form class="form-group" action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-md-3">
						<label for="genre">Genre</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="category_name" id="genre">
						{{ ($errors->has('category_name')) ? $errors->first('category_name') : "" }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3">
						<label for="description">Deskripsi</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="description" id="description">
						{{ ($errors->has('description')) ? $errors->first('description') : "" }}
					</div>
				</div>
				<br>
			<div class="row">
				<div class="col-md-3 offset-md-5 offset-md-4">
					<button class="btn btn-outline-primary" type="submit">Simpan</button>
				</div>
			</div>
		</form>
	</div>
	</div>
</div>
</div>
@endsection