@extends('templates.home')
@section('title')
	Edit Kategori
@endsection
@section('content')

	<div class="container">
		<h1>Edit Kategori</h1>
		<hr>

		<div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">

		<div class="card-header bg-primary text-white">
			<h5> Edit Category Detail </h5>
		</div>

		<div class="card-body">

		<div class="container text-primary">
			<form action="{{ route('categories.update', $categories['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">
				@csrf
				@method('PUT')

				    <div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="genre">Genre</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="category_name" id="genre" value="{{ $categories['category_name'] }}">
						{{ ($errors->has('category_name')) ? $errors->first('category_name') : "" }}
					</div>
					</div>
					<br>

					<div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="description">Deskripsi</label>
					</div>
					<div class="col-md-8">
						<input class="form-control" type="text" name="description" id="description" value="{{ $categories['description'] }}">
						{{ ($errors->has('description')) ? $errors->first('description') : "" }}
					</div>
					</div>
					<br>

					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Simpan</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endsection