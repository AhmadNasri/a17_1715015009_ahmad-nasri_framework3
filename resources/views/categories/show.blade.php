@extends('templates.home')
@section('title')
	Show Kategori
@endsection
@section('content')

	<h1>Detail Kategori</h1>
	<hr>
	<br>

	<div class="card bg-white border-info" style="max-width: 70%; margin: auto; min-height: 400px;">

		<div class="row">
			<div class="col-md-12 text-center">
				<h3>{{ $categories['category_name'] }} </h3>
			</div>
		</div>
		<hr>

		<br>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				ID
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $categories['id'] }}
			</div>
			<br>
		</div>

		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Genre
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $categories['category_name'] }}
			</div>		
		</div>


		<div class="row">
			<div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
				Deskripsi
			</div>
			<div class="col-md-4 col-sm-4">
				{{ $categories['description'] }}
			</div>		
		</div>
	</div>

@endsection