<!DOCTYPE html>
<html lang="en">
	@include('templates.partials._head')
<body>

	@include('templates.partials._topNav')

	<div class="container-fluid">

		<div class="row">

			@include('templates.partials._sideNav')

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				@yield('content')
			</main>

		</div>
	</div>

	<!--   -->
	
	@include('templates.partials._script')

</body>
</html>