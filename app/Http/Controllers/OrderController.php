<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;

class OrderController extends Controller
{

    public function index()
    {
        $orders = Order::with('users')->paginate(5);
        return view('orders.index',['orders'=>$orders]);
    }

    public function create()
    {
        $users = User::all();
        return view('orders.create', ['users'=>$users]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'invoice_number' => 'required|min:10|max:32',
            'total_price' => 'required|numeric',
            'status' => 'required'
        ]);

        Order::create([
            'user_id' => $request->user_id,
            'invoice_number' => $request->invoice_number,
            'total_price' => $request->total_price,
            'status' => $request->status
        ]);

        return redirect('/orders');
    }


    public function show($id)
    {
        $orders = Order::with('users')->find($id);
        return view('orders.show',['orders'=>$orders]);
    }

    public function edit($id)
    {
        $orders = Order::with('users')->find($id);
        $users = User::all();
        return view('orders.edit',['orders'=>$orders, 'users'=>$users]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'invoice_number' => 'required|min:10|max:32',
            'user_id' => 'required',
            'total_price' => 'required|numeric',
            'status' => 'required'
        ]);

        $orders = Order::find($id);
        $orders->invoice_number = $request->invoice_number;
        $orders->user_id = $request->user_id;
        $orders->total_price = $request->total_price;
        $orders->status = $request->status;
        $orders->save();

        return redirect('/orders');
    }

    public function destroy($id)
    {
        $orders = Order::find($id);
        $orders->delete();
        return redirect('/orders');
    }
    
    public function search(Request $request) {
        $cari = $request->cari;

        $orders = Order::where('status','LIKE','%'.$cari.'%')
                        ->orWhere('invoice_number','LIKE','%'.$cari.'%')->paginate();
        return view('orders.index', ['orders'=>$orders]);
    }
}
