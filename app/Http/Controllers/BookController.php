<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;
use App\Book_Category;

class BookController extends Controller
{

    public function index()
    {
        $books = Book::paginate(5);
        return view('books.index', ['books'=>$books]);
    }

    public function create()
    {
        $categories = Category::all();
        return view('books.create',['categories'=>$categories]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:191',
            'author' => 'required|string|max:191',
            'publisher' => 'required|max:191',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'description' => 'required|max:191',
            'category_name' => 'required',
            'cover' => 'required|image',
        ]);

        if($request->hasFile('cover')) {
            $gambar = $request->file('cover');
            $nama = '/picture/Book/'.time().'.'.$gambar->getClientOriginalExtension();
            $direktori = public_path('/picture/Book');
            $gambar->move($direktori, $nama);
        }

        Book::create([
            'title' => $request->title,
            'author' => $request->author,
            'publisher' => $request->publisher,
            'price' => $request->price,
            'stock' => $request->stock,
            'description' => $request->description,
            'cover' => $nama,
        ]);

        $books = Book::latest()->first();

        foreach($request->category_name as $genre) {
            Book_Category::create([
                'book_id' => $books->id,
                'category_id' => $genre
            ]);
        }
        
        return redirect('/books');
    }

    public function show($id)
    {
        $books = Book::find($id);
        return view('books.show', ['books'=>$books]);
    }

    public function edit($id)
    {
        $books = Book::find($id);
        $categories = Category::all();
        return view('books.edit', ['books'=>$books, 'categories'=>$categories]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:191',
            'author' => 'required|string|max:191',
            'publisher' => 'required|max:191',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'description' => 'required|max:191',
            'category_name' => 'required'
        ]);

        $books = Book::find($id);
        $books->title = $request->title;
        $books->author = $request->author;
        $books->publisher = $request->publisher;
        $books->price = $request->price;
        $books->stock = $request->stock;
        $books->description = $request->description;

        if($request->hasFile('cover')) {
            $gambar = $request->file('cover');
            $nama = '/picture/Book/'.time().'.'.$gambar->getClientOriginalExtension();
            $direktori = public_path('/picture/Book');
            $gambar->move($direktori, $nama);
        } else {
            $nama = $books->cover;
        }

        $books->cover = $nama;
        $books->save();

        $books_categories = Book_Category::where('book_id', $id)->delete();

        foreach($request->category_name as $genre) {
            Book_Category::create([
                'book_id' => $id,
                'category_id' => $genre
            ]);
        }

        return redirect('/books');
    }

    public function destroy($id)
    {
        $books = Book::find($id);
        $books->delete();
        return redirect('/books');
    }

    public function search(Request $request) {
        $cari = $request->cari;

        $books =  Book::where('title','LIKE','%'.$cari.'%')->paginate();
        return view('books.index', ['books'=>$books]);
    }
}
