<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    public function index()
    {
        $users = User::paginate(5);
        return view('users.index',['users'=>$users]);
    }

    public function create()
    {
        return view('users.create');
    }

     public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:8|max:32',
            'alamat' => 'required|max:50',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6|max:32',
            'telepon' => 'required|min:11|max:13',
            'avatar' => 'required|image'
        ]);

        if($request->hasFile('avatar')) {
            $gambar = $request->file('avatar');
            $nama = '/picture/User/'.time().'.'.$gambar->getClientOriginalExtension();
            $direktori = public_path('/picture/User');
            $gambar->move($direktori, $nama);
        }

        User::create([
            'username' => $request->nama,
            'address' => $request->alamat,
            'status' => "ACTIVE",
            'email' => $request->email,
            'password' => $request->password,
            'phone' => $request->telepon,
            'avatar' => $nama
        ]);
     
        return redirect('/users');
    }

    public function show($id)
    {
        $users = User::find($id);
        return view('users.show',['users'=>$users]);
    }

    public function edit($id)
    {
        $users = User::find($id);
        return view('users.edit',['users'=>$users]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|min:8|max:32',
            'alamat' => 'required|max:50',
            'email' => 'required|email',
            'telepon' => 'required|numeric'
        ]);

        $users = User::find($id);
        $users->username = $request->nama;
        $users->address = $request->alamat;
        $users->email = $request->email;
        $users->phone = $request->telepon;

        if($request->hasFile('avatar')) {
            $gambar = $request->file('avatar');
            $nama = '/picture/User/'.time().'.'.$gambar->getClientOriginalExtension();
            $direktori = public_path('/picture/User');
            $gambar->move($direktori, $nama);
        } else {
            $nama = $users->avatar;
        }

        $users->avatar = $nama;
        $users->save();

        return redirect('/users');
    }

    public function destroy($id)
    {
        $users = User::find($id);
        $users->delete();
        return redirect('/users');
    }  
    
    public function search(Request $request) {
        $cari = $request->cari;

        $users = User::where('email','LIKE', '%'.$cari.'%')->paginate();
        return view('users.index', ['users'=>$users]);
    }
}
