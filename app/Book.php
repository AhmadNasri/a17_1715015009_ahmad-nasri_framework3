<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'id', 'title', 'description',
        'author', 'publisher', 'cover',
        'price', 'stock'
    ];

    public function category() {
        return $this->belongsToMany('App\Category');
    }
}
