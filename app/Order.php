<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'id', 'user_id', 'total_price',
        'invoice_number', 'status'
    ];

    public function users() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
