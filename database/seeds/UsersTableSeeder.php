<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id'=>'1',
                'username'=>'Ahmad Nasri',
                'email'=>'ahmadnasri@gmail.com',   
                'address'=>'jl. Perjuangan',
                'phone'=>'082256487653',
                'status'=>'INACTIVE',
                'avatar'=>'/picture/User/1.jpg',
                'password'=>'kalorxorang'
            ],
            [
                'id'=>'2',
                'username'=>'Wisnu Angga Dinata',
                'email'=>'wisnuangga@gmail.com',
                'address'=>'jl. Gatot Subroto',
                'phone'=>'082354678976',
                'status'=>'INACTIVE',
                'avatar'=>'/picture/User/2.jpg',
                'password'=>'anggadinata'            
            ],
            [
                'id'=>'3',
                'username'=>'Muhammad Aryandi',
                'email'=>'muhammadaryandi@gmail.com',
                'address'=>'jl. diponegoro',
                'phone'=>'082278652345',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/3.jpg',
                'password'=>'mariandi'            
            ],
            [
                'id'=>'4',
                'username'=>'Safruddin',
                'email'=>'safruddin123@gmail.com',
                'address'=>'jl. Perjuangan',
                'phone'=>'085342678976',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/4.jpg',
                'password'=>'maumereende'            
            ],
            [
                'id'=>'5',
                'username'=>'Muhammad Imron Atma Wijaya',
                'email'=>'imronatma123@gmail.com',
                'address'=>'jl. panjaitan',
                'phone'=>'085323127896',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/5.jpg',
                'password'=>'achisub'        
            ],
            [
                'id'=>'6',
                'username'=>'Abdul Hanan',
                'email'=>'abdulhanan@gmail.com',
                'address'=>'jl. Pm. Noor',
                'phone'=>'082252589008',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/6.jpg',
                'password'=>'hanannan'        
            ],
            [
                'id'=>'7',
                'username'=>'Zulmansyah',
                'email'=>'zulmansyah@gmail.com',
                'address'=>'jl. Suparman',
                'phone'=>'085387907654',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/7.jpg',
                'password'=>'zulmanat'        
            ],
            [
                'id'=>'8',
                'username'=>'Dede Reni',
                'email'=>'dedereni@gmail.com',
                'address'=>'jl. Citarum',
                'phone'=>'082213546789',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/8.jpg',
                'password'=>'baasyir'        
            ],
            [
                'id'=>'9',
                'username'=>'Rahmawati',
                'email'=>'rahmawati@gmail.com',
                'address'=>'jl. Mulawarman',
                'phone'=>'081345267890',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/9.jpg',
                'password'=>'mularahma'        
            ],
            [
                'id'=>'10',
                'username'=>'Muhammad Samir',
                'email'=>'muhammadsamir@gmail.com',
                'address'=>'jl. Agung',
                'phone'=>'087829102388',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/10.jpg',
                'password'=>'poetrabone'        
            ],
            [
                'id'=>'11',
                'username'=>'Muhammad Iqbal',
                'email'=>'muhammadiqbal@gmail.com',
                'address'=>'jl. Merapi',
                'phone'=>'085342876543',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/11.jpg',
                'password'=>'alarabi'        
            ],
            [
                'id'=>'12',
                'username'=>'Rika Irma Diyanti',
                'email'=>'rikairma@gmail.com',
                'address'=>'jl. Anggrek',
                'phone'=>'081390876543',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/12.jpg',
                'password'=>'rikadiyanti'        
            ],
            [
                'id'=>'13',
                'username'=>'Indah Tika',
                'email'=>'indahtika@gmail.com',
                'address'=>'jl. Citarum',
                'phone'=>'081289997102',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/13.jpg',
                'password'=>'tikaah'        
            ],
            [
                'id'=>'14',
                'username'=>'Udin',
                'email'=>'udin5656@gmail.com',
                'address'=>'jl. Tendean',
                'phone'=>'082217881032',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/14.jpg',
                'password'=>'udinblack'        
            ],
            [
                'id'=>'15',
                'username'=>'Naya',
                'email'=>'naya5667@gmail.com',
                'address'=>'jl. Diponegoro',
                'phone'=>'081298768970',
                'status'=>'ACTIVE',
                'avatar'=>'/picture/User/15.jpg',
                'password'=>'nayadoank'        
            ]
        ]);
    }
}
