<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'id'=>'1',
                'title'=>'Nadi Hang Tuah',
                'description'=>'Pada zaman dahulu kala, dikenal seorang kesatria bernama Hang Tuah.',
                'author'=>'Abdul Kadir Ibrahim',
                'publisher'=>'Gramedia',
                'cover'=>'/picture/Book/1.jpg',
                'price'=>'250000',
                'stock'=>'30'
            ],
            [
                'id'=>'2',
                'title'=>'Misteri Sebuah Peti Mati',
                'description'=>'Kisah Ajun Komisaris Bursok mengungkap teror peti mati terus berlanjut.',
                'author'=>'Abdullah Harahap',
                'publisher'=>'Paradoks',
                'cover'=>'/picture/Book/2.jpg',
                'price'=>'355000',
                'stock'=>'10'
            ],
            [
                'id'=>'3',
                'title'=>'Geni Jora',
                'description'=>'Matanya belok, seperti boneka cantik dari negeri Antah (Heran, kenapa ya tokoh wanita dalam novel dan film selalu saja dikatakan cantik.',
                'author'=>'Abidah El Khalieqy',
                'publisher'=>'Q-Med',
                'cover'=>'/picture/Book/3.jpg',
                'price'=>'345000',
                'stock'=>'10'
            ],
            [
                'id'=>'4',
                'title'=>'Kumpulan Lukisan',
                'description'=>'Akan tetapi,Irfan sangat bersedih mendengar Bu Mutia tiba-tiba jatuh sakit dan di rawat di rumah sakit.',
                'author'=>'Abu Bakar',
                'publisher'=>'Koleksi Lukisan',
                'cover'=>'/picture/Book/4.jpg',
                'price'=>'230000',
                'stock'=>'11'
            ],
            [
                'id'=>'5',
                'title'=>'Dolanan',
                'description'=>'Enam orang anak tiba-tiba datang mengerumuni gerobak mainan pakde. Jaring panjang diambil, raket pun direbut.',
                'author'=>'Abu Bakar',
                'publisher'=>'Kumpulan Puisi',
                'cover'=>'/picture/Book/5.jpg',
                'price'=>'600000',
                'stock'=>'12'
            ],
            [
                'id'=>'6',
                'title'=>'Jika Kamu Menjadi Tanda Tambah',
                'description'=>'Buku ini mengajarkan konsep penjumlahan dalam matematika kepada anak-anak.',
                'author'=>'Trista Speed Shaskan',
                'publisher'=>'BIP',
                'cover'=>'/picture/Book/6.jpg',
                'price'=>'450000',
                'stock'=>'10'
            ],
            [
                'id'=>'7',
                'title'=>'Beruang Kutub dan Panda',
                'description'=>'Ada dua ekor beruang. Yang satu adalah beruang panda, yang satu lagi beruang kutub',
                'author'=>'Matthew J.baek',
                'publisher'=>'Kid Classic',
                'cover'=>'/picture/Book/7.jpg',
                'price'=>'950000',
                'stock'=>'7'
            ],
            [
                'id'=>'8',
                'title'=>'Pierre',
                'description'=>'Sosok Pierre Andries Tendean kerap disebut setiap harinya, entah sebagai nama jalan, gedung ataupun simbol militer.',
                'author'=>'Gustavo Mazali',
                'publisher'=>'Pt Gramedia Pustaka',
                'cover'=>'/picture/Book/8.jpg',
                'price'=>'750000',
                'stock'=>'3'
            ],
            [
                'id'=>'9',
                'title'=>'Piknik Seru',
                'description'=>'Lala dan beberapa teman sekelasnya akan pergi piknik di taman kota.',
                'author'=>'Anita Hairunnisa',
                'publisher'=>'Little Serambi',
                'cover'=>'/picture/Book/9.jpg',
                'price'=>'600000',
                'stock'=>'10'
            ],
            [
                'id'=>'10',
                'title'=>'Tetap Sehat Setelah Usia 40',
                'description'=>'Tetap sehat setelah usia 40, mungkinkah? Mungkin! Sangat mungkin bahkan. Apa rahasianya?
',
                'author'=>'dr. Salma',
                'publisher'=>'Gema Insani',
                'cover'=>'/picture/Book/10.jpg',
                'price'=>'560000',
                'stock'=>'5'
            ],
            [
                'id'=>'11',
                'title'=>'Jejak-jejak Cinta',
                'description'=>'Bagi setiap lelaki yang menjadi seorang ayah',
                'author'=>'Toni Raharjo',
                'publisher'=>'Gema Insani',
                'cover'=>'/picture/Book/11.jpg',
                'price'=>'760000',
                'stock'=>'6'
            ],
            [
                'id'=>'12',
                'title'=>'Kisah 7 Bayi Bisa Bicara',
                'description'=>'Atas kekuasaan Allah, ada tujuh bayi yang bisa berbicara dengan fasih layaknya orang dewasa. Mereka masih dalam buaian.',
                'author'=>'Uwais Ramadhan,Lc',
                'publisher'=>'Gema Insani',
                'cover'=>'/picture/Book/12.jpg',
                'price'=>'975000',
                'stock'=>'11'
            ],
            [
                'id'=>'13',
                'title'=>'Jomblos Diary',
                'description'=>'Anggapan bahwa punya pacar/gebetan itu gaul dan keren sedangkan jomblo itu kuper dan tidak laku-laku',
                'author'=>'O. Solihin',
                'publisher'=>'Gema Insani',
                'cover'=>'/picture/Book/13.jpg',
                'price'=>'830000',
                'stock'=>'4'
            ],
            [
                'id'=>'14',
                'title'=>'Mengisi Hati di Lorong Kehidupan 2',
                'description'=>'Buku ini berusaha menjadi jembatan yang menyediakan jalan bagi jiwa untuk bermuhasabah melalui kisah-kisah yang tersaji.',
                'author'=>'Ibnu Basyar',
                'publisher'=>'Gema Insani',
                'cover'=>'/picture/Book/14.jpg',
                'price'=>'470000',
                'stock'=>'1'
            ],
            [
                'id'=>'15',
                'title'=>'Kesepaduan Iman dan Amal Saleh',
                'description'=>'Untuk menyeimbangkan hidup sesuai tuntunan Islam.',
                'author'=>'Prof. Dr. Hamka',
                'publisher'=>'Gema Insani',
                'cover'=>'/picture/Book/15.jpg',
                'price'=>'350000',
                'stock'=>'4'
            ]
            
        ]);
    }
}
