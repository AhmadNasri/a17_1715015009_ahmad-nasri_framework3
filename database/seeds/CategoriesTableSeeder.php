<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'id'=>'1',
                'category_name'=>'Legenda',
                'description'=>'Cerita Rakyat'
            ],
            [
                'id'=>'2',
                'category_name'=>'Dongeng',
                'description'=>'Imajinasi'
            ],
            [
                'id'=>'3',
                'category_name'=>'Petualang',
                'description'=>'Penyemangat'
            ],
            [
                'id'=>'4',
                'category_name'=>'Cinta' ,
                'description'=>'Kisah Cinta'
            ],
            [
                'id'=>'5',
                'category_name'=>'Petualang',
                'description'=>'Petualangan'
            ],
            [
                'id'=>'6',
                'category_name'=>'Motivasi',
                'description'=>'Penyemangat Hidup'
            ],
            [
                'id'=>'7',
                'category_name'=>'Binatang',
                'description'=>'Budidaya'
            ],
            [
                'id'=>'8',
                'category_name'=>'Masyarakat',
                'description'=>'Sosial'
            ],
            [
                'id'=>'9',
                'category_name'=>'petualang',
                'description'=>'Petualang'
            ],
            [
                'id'=>'10',
                'category_name'=>'Kedokteran',
                'description'=>'Kesehatan'
            ],
            [
                'id'=>'11',
                'category_name'=>'Cinta',
                'description'=>'Kasih Sayang'
            ],
            [
                'id'=>'12',
                'category_name'=>'Dongeng',
                'description'=>'Imajinasi'
            ],
            [
                'id'=>'13',
                'category_name'=>'Cinta',
                'description'=>'Kasih Sayang'
            ],
            [
                'id'=>'14',
                'category_name'=>'Religi',
                'description'=>'Keimanan'
            ],
            [
                'id'=>'15',
                'category_name'=>'Religi',
                'description'=>'Penyemangat Hidup'
            ]
        ]);
    }
}
