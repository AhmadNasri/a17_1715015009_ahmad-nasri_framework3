<?php

Route::resource('users', 'UserController');
Route::resource('books', 'BookController');
Route::resource('categories', 'CategoryController');
Route::resource('orders', 'OrderController');

Route::get('/users/search/email', 'UserController@search')->name('users.search');
Route::get('/books/search/title', 'BookController@search')->name('books.search');
Route::get('/categories/search/genre', 'CategoryController@search')->name('categories.search');
Route::get('/orders/search/pesanan', 'OrderController@search')->name('orders.search');
